﻿using System;

namespace BuyIt.Exceptions
{
    public class MenuException : Exception
    {
        public MenuException() { }
        public MenuException(string message) : base(message) { }
        public MenuException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class MenuNotLoadedException : MenuException
    {
        public MenuNotLoadedException() { }
        public MenuNotLoadedException(string message) : base(message) { }
        public MenuNotLoadedException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class MenuNotParsedException : MenuException
    {
        public MenuNotParsedException() { }
        public MenuNotParsedException(string message) : base(message) { }
        public MenuNotParsedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
