﻿using System;

namespace BuyIt.Exceptions
{
    public class MailSenderException : Exception
    {
        public MailSenderException() { }
        public MailSenderException(string message) : base(message) { }
        public MailSenderException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class CannotSendMailException : MailSenderException
    {
        public CannotSendMailException() { }
        public CannotSendMailException(string message) : base(message) { }
        public CannotSendMailException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class ConfigNotLoadedException : MailSenderException
    {
        public ConfigNotLoadedException() { }
        public ConfigNotLoadedException(string message) : base(message) { }
        public ConfigNotLoadedException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class ConfigNotParsedException : MailSenderException
    {
        public ConfigNotParsedException() { }
        public ConfigNotParsedException(string message) : base(message) { }
        public ConfigNotParsedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
