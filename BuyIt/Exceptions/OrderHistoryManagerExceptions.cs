﻿using System;

namespace BuyIt.Exceptions
{
    public class OrderHistoryManagerException : Exception
    {
        public OrderHistoryManagerException() { }
        public OrderHistoryManagerException(string message) : base(message) { }
        public OrderHistoryManagerException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class OrderHistoryManagerNotLoadedException : OrderHistoryManagerException
    {
        public OrderHistoryManagerNotLoadedException() { }
        public OrderHistoryManagerNotLoadedException(string message) : base(message) { }
        public OrderHistoryManagerNotLoadedException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class OrderHistoryManagerNotParsedException : OrderHistoryManagerException
    {
        public OrderHistoryManagerNotParsedException() { }
        public OrderHistoryManagerNotParsedException(string message) : base(message) { }
        public OrderHistoryManagerNotParsedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
