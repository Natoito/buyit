﻿using System;

namespace BuyIt.Exceptions
{
    public class XmlLoaderException : Exception
    {
        public XmlLoaderException() { }
        public XmlLoaderException(string message) : base(message) { }
        public XmlLoaderException(string message, Exception innerException) : base(message, innerException) { }
    }
}
