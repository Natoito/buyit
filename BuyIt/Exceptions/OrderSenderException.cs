﻿using System;

namespace BuyIt.Exceptions
{
    public class OrderSenderException : Exception
    {
        public OrderSenderException() { }
        public OrderSenderException(string message) : base(message) { }
        public OrderSenderException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class CannotSendOrderException : MailSenderException
    {
        public CannotSendOrderException() { }
        public CannotSendOrderException(string message) : base(message) { }
        public CannotSendOrderException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class OrderSenderConfigNotLoadedException : OrderSenderException
    {
        public OrderSenderConfigNotLoadedException() { }
        public OrderSenderConfigNotLoadedException(string message) : base(message) { }
        public OrderSenderConfigNotLoadedException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class OrderSenderConfigNotParsedException : OrderSenderException
    {
        public OrderSenderConfigNotParsedException() { }
        public OrderSenderConfigNotParsedException(string message) : base(message) { }
        public OrderSenderConfigNotParsedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
