﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BuyIt
{
    public static class DeepClone
    {
        public static T CopyObject<T>(T objSource)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, objSource);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
