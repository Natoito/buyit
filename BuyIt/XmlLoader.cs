﻿using BuyIt.Exceptions;

using System;
using System.IO;
using System.Xml;

namespace BuyIt
{
    public class XmlLoader
    {
        public static XmlDocument LoadFromFile(string path)
        {
            FileStream fileStream  = null;
            XmlDocument parsedData = null;

            try
            {
                fileStream = new FileStream(path, FileMode.Open);

                parsedData = new XmlDocument();

                parsedData.Load(fileStream);
            }
            catch (XmlException e)
            {
                throw new XmlLoaderException("File not parsed", e);
            }
            catch (IOException e)
            {
                throw new XmlLoaderException("Cannot open file", e);
            }
            catch (Exception e)
            {
                throw new XmlLoaderException("Unknown error", e);
            }
            finally
            {
                if (fileStream != null)
                {
                    fileStream.Close();
                }
            }

            return parsedData;
        }
    }
}
