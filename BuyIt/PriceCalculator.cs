﻿using BuyIt.Dishes;

using System;

namespace BuyIt
{
    [Serializable]
    class PriceCalculator
    {
        public float CalculateOrderPrice(Order order)
        {
            float price = 0.0f;

            foreach (Dish dish in order.Dishes)
            {
                dish.TotalPrice = CalculateDishPrice(order, dish);
                price += dish.TotalPrice;
            }

            return price;
        }

        public float CalculateDishPrice(Order order, Dish dish)
        {
            float price = 0.0f;

            price += dish.Price;

            if (dish is ExtendedDish)
            {
                ExtendedDish extendedDish = (ExtendedDish)dish;

                foreach (Dish extra in extendedDish.Extras)
                {
                    price += extra.Price;
                }
            }

            return price;
        }
    }
}
