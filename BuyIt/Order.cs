﻿using BuyIt.Dishes;

using System;

namespace BuyIt
{
    [Serializable]
    public class Order : OrderData
    {
        private PriceCalculator priceCalculator;

        public Order()
        {
            this.priceCalculator = new PriceCalculator();
        }

        public void AddDish(Dish dish)
        {
            if(dish != null)
            {
                this.Dishes.Add(DeepClone.CopyObject(dish));

                this.TotalPrice = this.priceCalculator.CalculateOrderPrice(this);
            }
            else
            {
                throw new ArgumentNullException("Dish is null");
            }

        }

        public void UpdateDate()
        {
            this.Date = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss");
        }

        public void RemoveDishByIndex(int index)
        {
            if ((index >= 0) && 
                (index < this.Dishes.Count))
            {
                this.Dishes.RemoveAt(index);

                this.TotalPrice = this.priceCalculator.CalculateOrderPrice(this);
            }
            else
            {
                throw new IndexOutOfRangeException("Dish index out of range");
            }
        }

        public float CalculateDishPrice(Dish dish)
        {
            if(dish != null)
            {
                return this.priceCalculator.CalculateDishPrice(this, dish);
            }
            else
            {
                throw new ArgumentNullException("Dish is null");
            }
        }

        public void CleanOrder()
        {
            this.Dishes.Clear();
        }
    }
}
