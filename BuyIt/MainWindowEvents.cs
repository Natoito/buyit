﻿using BuyIt.Dishes;

using System.Collections.Generic;

namespace BuyIt
{
    public delegate void OnDishRemovedFromOrder(List<bool> dishesToRemove);
    public delegate void OnDishUnselected();
    public delegate void OnSelectedDishUpdated(Dish selectedDish);
    public delegate void OnDishAddedToOrder(Dish selectedDish);
    public delegate void OnSendOrderButtonClicked(string commentsToRder);
}
