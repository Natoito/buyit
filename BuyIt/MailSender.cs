﻿using BuyIt.Exceptions;

using System;
using System.Xml;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BuyIt
{
    class MailSender
    {
        /*--------------- public events ---------------*/

        public event OnSendMailCompleted SendMailCompleted;

        /*--------------- public variables ---------------*/

        private string configFilePath;

        private int smtpPort;
        private string smtpAddress;

        private string senderAddress;
        private string senderPassword;

        private SmtpClient smtpClient;

        /*--------------- public methods ---------------*/

        /// <summary>
        /// Default construction.
        /// </summary>
        public MailSender()
        {
            this.smtpPort = 0;

            this.smtpClient     = null;
            this.smtpAddress    = null;
            this.senderAddress  = null;
            this.senderPassword = null;
        }

        /// <summary>
        /// Initialization MailSender with path to config.
        /// </summary>
        /// <param name="configFilePath"> Path to config. </param>
        public MailSender(string configFilePath)
            :
            this()
        {
            Debug.Assert(configFilePath != null, "Path is null");

            this.configFilePath = configFilePath;
        }

        /// <summary>
        /// Mail sender initialization.
        /// <exception cref="ConfigNotLoadedException"> Thrown when file is not loaded </exception>
        /// <exception cref="ConfigNotParsedException"> Thrown when file is not parsed </exception>
        /// </summary>
        public void Init()
        {
            try
            {
                XmlDocument data = XmlLoader.LoadFromFile(this.configFilePath);

                if (data != null)
                {
                    this.ParseData(data);
                    this.InitSmtpClient();
                }
            }
            catch (XmlLoaderException e)
            {
                throw new ConfigNotLoadedException("Config not loaded", e);
            }
            catch(ConfigNotParsedException e)
            {
                throw new ConfigNotParsedException("Config not parsed", e);
            }
        }

        /// <summary>
        /// Asynchronous sending mail.
        /// </summary>
        /// <param name="addressees"> Addressees </param>
        /// <param name="title"> Title of message </param>
        /// <param name="message"> Message </param>
        /// <exception cref="CannotSendMailException"> Thrown when sending e-mail failed </exception>
        /// <returns> Returns task. </returns>
        public async Task SendMail(List<string> addressees, string title, string message)
        {
            MailMessage mailMessage = new MailMessage();

            mailMessage.From = new MailAddress(this.senderAddress);
            mailMessage.Body = message;
            mailMessage.Subject = title;

            foreach(string addressee in addressees)
            {
                mailMessage.To.Add(addressee);
            }

            try
            {
                await this.smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception e)
            {
                throw new CannotSendMailException("Cannot send mail", e);
            }
        }

        /*--------------- private methods ---------------*/

        private void ParseData(XmlDocument data)
        {
            if(data.DocumentElement != null)
            {
                if (data.DocumentElement.Name.Equals("config"))
                {
                    try
                    {
                        foreach (XmlNode child in data.DocumentElement.ChildNodes)
                        {
                            if (child.Name.Equals("sender"))
                            {
                                this.ParseSenderData(child);
                            }

                            if (child.Name.Equals("smtp"))
                            {
                                this.ParseSmtpData(child);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }                 
                }
                else
                {
                    throw new ConfigNotParsedException("Config section not found");
                }
            }
            else
            {
                throw new ConfigNotParsedException("Data not parsed");
            }
        }
        private void ParseSenderData(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name.Equals("address"))
                {
                    this.senderAddress = (string)child.FirstChild.Value.Clone();
                }
                if (child.Name.Equals("password"))
                {
                    this.senderPassword = (string)child.FirstChild.Value.Clone();
                }
            }

            if(this.senderAddress == null)
            {
                throw new ConfigNotParsedException("Sender address not found");
            }

            if (this.senderPassword == null)
            {
                throw new ConfigNotParsedException("Sender password not found");
            }
        }
        private void ParseSmtpData(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name.Equals("address"))
                {
                    this.smtpAddress = (string)child.FirstChild.Value.Clone();
                }
                if (child.Name.Equals("port"))
                {
                    if(Int32.TryParse(child.FirstChild.Value, out this.smtpPort) == false)
                    {
                        throw new ConfigNotParsedException("Port not parsed");
                    }
                }
            }

            if (this.smtpAddress == null)
            {
                throw new ConfigNotParsedException("Smtp address not found");
            }
        }

        private void InitSmtpClient()
        {
            this.smtpClient = new SmtpClient(this.smtpAddress, this.smtpPort)
            {
                Credentials = new NetworkCredential(this.senderAddress, this.senderPassword),
                EnableSsl = true
            };

            this.smtpClient.SendCompleted += SmtpClient_SendCompleted;
        }

        private void SmtpClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if(this.SendMailCompleted != null)
            {
                this.SendMailCompleted(e.Error);
            }
        }
    }
}
