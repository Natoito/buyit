﻿using BuyIt.Dishes;
using System;
using System.Xml;
using System.Collections.Generic;
using BuyIt.Exceptions;
using System.Diagnostics;

namespace BuyIt
{
    class OrderHistoryManager
    {
        public List<OrderData> Orders { get; private set; }

        public string ordersHistroyPath;

        public OrderHistoryManager(string ordersHistroyPath)
        {
            this.ordersHistroyPath = ordersHistroyPath;

            this.Orders = new List<OrderData>();
        }

        public void LoadHistory()
        {
            try
            {
                XmlDocument data = null;

                data = XmlLoader.LoadFromFile(this.ordersHistroyPath);

                if (data != null)
                {
                    this.ParseData(data);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);

                this.Orders.Clear();

                this.SaveToFile();
            }
        }

        public void UpdateHistroy(OrderData orderData)
        {
            if(orderData != null)
            {
                this.Orders.Add(DeepClone.CopyObject(orderData));

                this.SaveToFile();
            }
            else
            {
                throw new ArgumentNullException("Order data is null");
            }
        }

        private void ParseData(XmlDocument data)
        {
            if (data.DocumentElement != null)
            {
                if (data.DocumentElement.Name.Equals("ordersHistory"))
                {
                    try
                    {
                        foreach (XmlNode child in data.DocumentElement.ChildNodes)
                        {
                            if (child.Name.Equals("order"))
                            {
                                this.ParseOrder(child);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                else
                {
                    throw new MenuNotParsedException("Menu section not found");
                }
            }
            else
            {
                throw new MenuNotParsedException("Data not parsed");
            }
        }

        private void ParseOrder(XmlNode node)
        {
            List<Dish> dishes = new List<Dish>();
            float totalPrice = 0.0f;

            string date     = string.Empty;
            string comments = string.Empty;

            if(node.Attributes.Count > 0)
            {
                if (node.Attributes.Count == 1)
                {
                    if (node.Attributes[0].Name.Equals("date"))
                    {
                        date = node.Attributes[0].Value;
                    }
                    else
                    {
                        throw new OrderHistoryManagerNotParsedException("Cannot find type attribute");
                    }
                }
                else
                {
                    throw new OrderHistoryManagerNotParsedException("To many attributes");
                }
            }

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("TotalPrice"))
                    {
                        if (float.TryParse(child.FirstChild.Value, out totalPrice) == false)
                        {
                            throw new MenuNotParsedException("Dish price not parsed");
                        }
                    }
                    if (child.Name.Equals("dish"))
                    {
                        Dish dish = null;

                        ParseDishNode(child, out dish);

                        if(dish != null)
                        {
                            dishes.Add(dish);
                        }
                        else
                        {
                            throw new MenuNotParsedException("Dish is null");
                        }
                    }
                    if (child.Name.Equals("Comments"))
                    {
                        comments = child.Value;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            this.Orders.Add(new OrderData(dishes, totalPrice, date, comments));
        }

        private void ParseDishNode(XmlNode node, out Dish dish)
        {
            string nodeType = string.Empty;

            if (node.Attributes.Count > 0)
            {
                if (node.Attributes.Count == 1)
                {
                    if (node.Attributes[0].Name.Equals("type"))
                    {
                        nodeType = node.Attributes[0].Value;
                    }
                    else
                    {
                        throw new OrderHistoryManagerNotParsedException("Cannot find type attribute");
                    }
                }
                else
                {
                    throw new OrderHistoryManagerNotParsedException("To many attributes");
                }
            }

            dish = null;

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("dishData"))
                    {
                        int id;
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out id, out name, out price);

                        if(nodeType.Equals("ExtendedDish"))
                        {
                            dish = new ExtendedDish(id, name, price);
                        }
                        else
                        {
                            dish = new Dish(id, name, price);
                        }
                    }
                    if (child.Name.Equals("extras"))
                    {
                        if(dish is ExtendedDish)
                        {
                            Dish extra = null;
                            ExtendedDish extendedDish = (ExtendedDish)dish;

                            ParseDishExtras(child, out extra);

                            if (extra != null)
                            {
                                extendedDish.Extras.Add(extra);
                            }
                            else
                            {
                                throw new MenuNotParsedException("Extra is null");
                            }
                        }
                        else
                        {
                            throw new OrderHistoryManagerNotParsedException("Found dishes extras but dish is not type of ExtendedDish");
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseDishExtras(XmlNode node, out Dish dish)
        {
            dish = null;

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("extra"))
                    {
                        int id;
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out id, out name, out price);

                        dish = new Dish(id, name, price);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseDish(XmlNode node, out int id, out string name, out float price)
        {
            id    = 0;
            name  = string.Empty;
            price = 0.0f;

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name.Equals("Id"))
                {
                    if (int.TryParse(child.FirstChild.Value, out id) == false)
                    {
                        throw new MenuNotParsedException("Dish id not parsed");
                    }
                }
                if (child.Name.Equals("Name"))
                {
                    name = (string)child.FirstChild.Value.Clone();
                }
                if (child.Name.Equals("Price"))
                {
                    if (float.TryParse(child.FirstChild.Value, out price) == false)
                    {
                        throw new MenuNotParsedException("Dish price not parsed");
                    }
                }
            }

            if (name.Equals(string.Empty))
            {
                throw new MenuNotParsedException("Name parameter not found");
            }
        }

        private void SaveToFile()
        {
            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement ordersHistoryNode = doc.CreateElement(string.Empty, "ordersHistory", string.Empty);

            foreach(OrderData orderData in this.Orders)
            {
                XmlElement orderNode      = doc.CreateElement(string.Empty, "order", string.Empty);
                XmlElement totalPriceNode = doc.CreateElement(string.Empty, "TotalPrice", string.Empty);

                XmlAttribute orderNodeDateAttribute = doc.CreateAttribute("date");

                orderNodeDateAttribute.InnerText = orderData.Date;
                orderNode.Attributes.Append(orderNodeDateAttribute);

                ordersHistoryNode.AppendChild(orderNode);

                totalPriceNode.InnerText = orderData.TotalPrice.ToString();

                orderNode.AppendChild(totalPriceNode);

                foreach(Dish dish in orderData.Dishes)
                {
                    orderNode.AppendChild(this.CreateDishNode(dish, doc));
                }

                if (orderData.Comments.Equals(string.Empty) == false)
                {
                    XmlElement commentsNode = doc.CreateElement(string.Empty, "Comments", string.Empty);

                    commentsNode.InnerText = orderData.Comments;

                    orderNode.AppendChild(commentsNode);
                }

                ordersHistoryNode.AppendChild(orderNode);
            }

            doc.AppendChild(ordersHistoryNode);

            doc.Save(this.ordersHistroyPath);
        }
        private XmlElement CreateDishNode(Dish dish, XmlDocument doc)
        {
            XmlElement dishNode = doc.CreateElement(string.Empty, "dish", string.Empty);

            XmlElement dishDataNode = doc.CreateElement(string.Empty, "dishData", string.Empty);

            XmlElement idNode         = doc.CreateElement(string.Empty, "Id", string.Empty);
            XmlElement nameNode       = doc.CreateElement(string.Empty, "Name", string.Empty);
            XmlElement priceNode      = doc.CreateElement(string.Empty, "Price", string.Empty);
            XmlElement totalPriceNode = doc.CreateElement(string.Empty, "TotalPrice", string.Empty);

            idNode.InnerText         = dish.Id.ToString();
            nameNode.InnerText       = dish.Name;
            priceNode.InnerText      = dish.Price.ToString();
            totalPriceNode.InnerText = dish.TotalPrice.ToString();

            dishDataNode.AppendChild(idNode);
            dishDataNode.AppendChild(nameNode);
            dishDataNode.AppendChild(priceNode);
            dishDataNode.AppendChild(totalPriceNode);

            dishNode.AppendChild(dishDataNode);

            if (dish is ExtendedDish)
            {
                ExtendedDish extendedDish = (ExtendedDish)dish;

                if(extendedDish.Extras.Count > 0)
                {
                    XmlElement extrasNode = doc.CreateElement(string.Empty, "extras", string.Empty);

                    XmlAttribute extendedDishAttribute = doc.CreateAttribute("type");

                    extendedDishAttribute.InnerText = "ExtendedDish";
                    dishNode.Attributes.Append(extendedDishAttribute);

                    foreach (Dish extra in extendedDish.Extras)
                    {
                        extrasNode.AppendChild(CreateExtrasNode(extra, doc));
                    }

                    dishNode.AppendChild(extrasNode);
                }
            }

            return dishNode;
        }
        private XmlElement CreateExtrasNode(Dish dish, XmlDocument doc)
        {
            XmlElement extraNode = doc.CreateElement(string.Empty, "extra", string.Empty);

            XmlElement idNode    = doc.CreateElement(string.Empty, "Id", string.Empty);
            XmlElement nameNode  = doc.CreateElement(string.Empty, "Name", string.Empty);
            XmlElement priceNode = doc.CreateElement(string.Empty, "Price", string.Empty);

            idNode.InnerText    = dish.Id.ToString();
            nameNode.InnerText  = dish.Name;
            priceNode.InnerText = dish.Price.ToString();

            extraNode.AppendChild(idNode);
            extraNode.AppendChild(nameNode);
            extraNode.AppendChild(priceNode);

            return extraNode;
        }
    }
}
