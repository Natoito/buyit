﻿using BuyIt.Dishes;
using BuyIt.Exceptions;

using System;
using System.Xml;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace BuyIt
{
    class OrderSender
    {
        /*--------------- private events ---------------*/

        public event OnSendOrderCompleted OnSendOrderCompleted;

        /*--------------- private variables ---------------*/

        private List<string> orderAddressee;
        private MailSender mailSender;
        private string addresseesPath;

        /*--------------- public methods ---------------*/

        /// <summary>
        /// Default construction.
        /// </summary>
        public OrderSender()
        {
            this.addresseesPath = "addressees.xml";
        }

        /// <summary>
        /// Initialization OrderSender with path to addressees.
        /// </summary>
        /// <param name="addresseesPath"> Path to addressees. </param>
        public OrderSender(string addresseesPath)
        {
            this.addresseesPath = addresseesPath;
        }

        /// <summary>
        /// Order sender initialization.
        /// <exception cref="OrderSenderException"> Thrown when cannot create mail sender </exception>
        /// <exception cref="OrderSenderConfigNotParsedException"> Thrown when cannot parse order sender config </exception>
        /// <exception cref="OrderSenderConfigNotLoadedException"> Thrown when cannot create order sender config </exception>
        /// </summary>
        public void Init()
        {
            this.orderAddressee = new List<string>();

            try
            {
                XmlDocument data = null;

                data = XmlLoader.LoadFromFile(this.addresseesPath);

                if (data != null)
                {
                    this.ParseData(data);
                }

                this.mailSender = new MailSender("orderSenderSmtpConfig.xml");
                this.mailSender.Init();

                this.mailSender.SendMailCompleted += MailSender_SendMailCompleted;
            }
            catch (MailSenderException e)
            {
                throw new OrderSenderException("Mail sender not created", e);
            }
            catch(OrderSenderConfigNotParsedException e)
            {
                throw new OrderSenderConfigNotParsedException("Order sender not parsed", e);
            }
            catch(XmlLoaderException e)
            {
                throw new OrderSenderConfigNotLoadedException("Order sender not loaded", e);
            }
        }

        /// <summary>
        /// Asynchronous sending order.
        /// </summary>
        /// <param name="order"></param>
        /// <exception cref="CannotSendOrderException"> Thrown when sending order failed </exception>
        /// <returns></returns>
        public async Task SendOrder(Order order)
        {
            try
            {
                await this.mailSender.SendMail(this.orderAddressee, "Zamówienie", this.OrderToString(order, order.Comments));
            }
            catch (Exception e)
            {
                throw new CannotSendOrderException("Cannot send order", e);
            }
        }

        /*--------------- private methods ---------------*/

        private void ParseData(XmlDocument data)
        {
            if (data.DocumentElement.Name.Equals("addressees"))
            {
                foreach (XmlNode child in data.DocumentElement.ChildNodes)
                {
                    if (child.Name.Equals("orderAddressee"))
                    {
                        this.orderAddressee.Add((string)child.FirstChild.Value.Clone());
                    }
                }
            }
            else
            {
                throw new OrderSenderConfigNotParsedException("Addressees not found");
            }

            if(this.orderAddressee.Count == 0)
            {
                throw new OrderSenderConfigNotParsedException("Cannot find any addressee");
            }
        }

        private string OrderToString(Order order, string commentsToOrder)
        {
            string ret = string.Empty;

            ret += "Zamówenie:";

            foreach (Dish dish in order.Dishes)
            {
                ret += "\n================================";
                ret += "\n" + dish.ToString();
            }

            if(commentsToOrder.Length > 0)
            {
                ret += "\n=======================================";
                ret += "\nUwagi do zamówenia:\n\n" + commentsToOrder;
            }
            
            return ret;
        }

        private void MailSender_SendMailCompleted(Exception error)
        {
            if (this.OnSendOrderCompleted != null)
            {
                this.OnSendOrderCompleted(error);
            }
        }
    }
}
