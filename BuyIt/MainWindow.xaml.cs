﻿using BuyIt.Dishes;

using System.Collections.Generic;

using System.Windows;
using System.Windows.Controls;

using System.Diagnostics;


namespace BuyIt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /*--------------- public events ---------------*/

        public event OnDishRemovedFromOrder DishRemovedFromOrder;
        public event OnDishUnselected DishUnselected;
        public event OnSelectedDishUpdated SelectedDishUpdated;
        public event OnDishAddedToOrder DishAddedToOrder;
        public event OnSendOrderButtonClicked SendOrderButtonClicked;

        /*--------------- private variables ---------------*/

        private Dish selectedDish;
        private float actualPrice;

        private TabItem actualSelectedKindOfDishTabItem;

        /*--------------- public methods ---------------*/

        public MainWindow()
        {
            InitializeComponent();

            this.actualPrice = 0.0f;

            this.PrepareControlsToNextOrder();

            this.actualSelectedKindOfDishTabItem = null;

            this.UpdatePrice(0.0f);
        }

        public void FillHistory(List<OrderData> orders)
        {
            this.ordersHistoryListView.ItemsSource = null;
            this.ordersHistoryListView.ItemsSource = orders;
        }

        public void FillPizzas(List<ExtendedDish> pizzas)
        {
            this.pizzasListView.ItemsSource = null;
            this.pizzasListView.ItemsSource = pizzas;
        }

        public void FillPizzasExtras(List<Dish> pizzaExtras)
        {
            this.pizzaExtrasListView.ItemsSource = null;
            this.pizzaExtrasListView.ItemsSource = pizzaExtras;
        }

        public void FillMainDishes(List<ExtendedDish> mainDishes)
        {
            this.mainDishesListView.ItemsSource = null;
            this.mainDishesListView.ItemsSource = mainDishes;
        }

        public void FillMainDishesExtras(List<Dish> mainDishesExtras)
        {
            this.mainDishesExtrasListView.ItemsSource = null;
            this.mainDishesExtrasListView.ItemsSource = mainDishesExtras;
        }

        public void FillSoups(List<Dish> soups)
        {
            this.soupsListView.ItemsSource = null;
            this.soupsListView.ItemsSource = soups;
        }

        public void FillDrinks(List<Dish> drinks)
        {
            this.drinksListView.ItemsSource = null;
            this.drinksListView.ItemsSource = drinks;
        }

        public void UpdateOrderList(Order order)
        {
            this.orderListView.ItemsSource = null;
            this.orderListView.ItemsSource = order.Dishes;

            this.soupsListView.UnselectAll();
            this.drinksListView.UnselectAll();

            this.mainDishesListView.UnselectAll();
            this.mainDishesExtrasListView.UnselectAll();

            this.pizzasListView.UnselectAll();
            this.pizzaExtrasListView.UnselectAll();

            if (order.Dishes.Count == 0)
            {
                this.removeDishButton.IsEnabled = false;
            }

            this.sendOrderButton.IsEnabled = true;
        }

        public void UpdatePrice(float price)
        {
            this.actualPrice = price;

            this.priceLabel.Content = "Do zapłaty: " + price + " zł.";
        }

        public void UpdatePriceToAdd(float lastPrice, float priceToAdd)
        {
            this.priceLabel.Content = "Do zapłaty: " + lastPrice + " + " + priceToAdd + " = " + (lastPrice + priceToAdd).ToString() + " zł.";
        }

        public void PrepareToNextOrder()
        {
            this.IsEnabled = true;

            this.PrepareControlsToNextOrder();
        }

        /*--------------- private methods ---------------*/

        private void PrepareControlsToNextOrder()
        {
            this.addDishButton.IsEnabled            = false;
            this.sendOrderButton.IsEnabled          = false;
            this.removeDishButton.IsEnabled         = false;
            this.pizzaExtrasListView.IsEnabled      = false;
            this.mainDishesExtrasListView.IsEnabled = false;

            this.orderListView.ItemsSource = null;

            this.commentsToOrder.Text = null;

            this.UpdatePrice(0.0f);
        }

        private void sendOrderButton_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;

            if (this.SendOrderButtonClicked != null)
            {
                this.SendOrderButtonClicked(this.commentsToOrder.Text);
            }

            this.selectedDish = null;

            this.soupsListView.UnselectAll();
            this.drinksListView.UnselectAll();
            this.pizzasListView.UnselectAll();
            this.mainDishesListView.UnselectAll();
            this.pizzaExtrasListView.UnselectAll();
            this.mainDishesExtrasListView.UnselectAll();

            this.UpdatePrice(this.actualPrice);
        }

        private void addDishButton_Click(object sender, RoutedEventArgs e)
        {
            this.addDishButton.IsEnabled            = false;
            this.pizzaExtrasListView.IsEnabled      = false;
            this.mainDishesExtrasListView.IsEnabled = false;

            if (this.DishAddedToOrder != null)
            {
                this.DishAddedToOrder(this.selectedDish);
            }

            this.selectedDish = null;
        }

        private void kindsOfFoodsTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.actualSelectedKindOfDishTabItem != this.kindsOfFoodsTabControl.SelectedItem)
            {
                this.actualSelectedKindOfDishTabItem = (TabItem)this.kindsOfFoodsTabControl.SelectedItem;

                this.selectedDish = null;

                this.pizzasListView.UnselectAll();
                this.pizzaExtrasListView.UnselectAll();
                this.mainDishesListView.UnselectAll();
                this.mainDishesExtrasListView.UnselectAll();
                this.soupsListView.UnselectAll();
                this.drinksListView.UnselectAll();

                this.UpdatePrice(this.actualPrice);
             
                this.addDishButton.IsEnabled            = false;
                this.pizzaExtrasListView.IsEnabled      = false;
                this.mainDishesExtrasListView.IsEnabled = false;

                if (this.DishUnselected != null)
                {
                    this.DishUnselected();
                }
            }
            else
            {
                e.Handled = true;
            }
        }


        private void pizzasListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.pizzasListView.SelectedItem != null)
            {
                this.addDishButton.IsEnabled       = true;
                this.pizzaExtrasListView.IsEnabled = true;

                this.selectedDish = DeepClone.CopyObject((Dish)this.pizzasListView.SelectedItem);

                this.pizzaExtrasListView.UnselectAll();

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }
            }
        }

        private void pizzaExtrasListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.pizzasListView.SelectedItems.Count > 0)
            {
                Debug.Assert(this.selectedDish != null, "Selected dish is null");

                ExtendedDish selectedPizza = this.selectedDish as ExtendedDish;

                Debug.Assert(selectedPizza != null, "Selected dish is not type of ExtendedDish");

                selectedPizza.Extras.Clear();

                foreach (Dish dish in this.pizzaExtrasListView.SelectedItems)
                {
                    selectedPizza.Extras.Add(DeepClone.CopyObject(dish));
                }

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }
            }
        }

        private void mainDishesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.mainDishesListView.SelectedItem != null)
            {
                this.selectedDish = DeepClone.CopyObject((Dish)this.mainDishesListView.SelectedItem);

                ExtendedDish dish = this.selectedDish as ExtendedDish;

                Debug.Assert(dish != null, "Selected Dish is not of type ExtendedDish");

                this.mainDishesExtrasListView.IsEnabled = true;

                this.mainDishesExtrasListView.UnselectAll();

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }

                this.addDishButton.IsEnabled = true;
            }
        }

        private void mainDishesExtrasListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.mainDishesExtrasListView.SelectedItems.Count > 0)
            {
                Debug.Assert(this.selectedDish != null, "Selected dish is null");

                ExtendedDish selectedMainDish = this.selectedDish as ExtendedDish;

                Debug.Assert(selectedMainDish != null, "Selected main dish is not type of ExtendedDish");

                selectedMainDish.Extras.Clear();

                foreach (Dish dish in this.mainDishesExtrasListView.SelectedItems)
                {
                    selectedMainDish.Extras.Add(DeepClone.CopyObject(dish));
                }

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }
            }
        }

        private void removeDishButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.DishRemovedFromOrder != null)
            {
                List<bool> dishesToRemove = new List<bool>();

                for(int i = 0; i < this.orderListView.Items.Count; i++)
                {
                    object selectedEntry = (object)this.orderListView.Items[i];
                    ListBoxItem item = this.orderListView.ItemContainerGenerator.ContainerFromItem(selectedEntry) as ListBoxItem;

                    if ((item != null) && item.IsSelected)
                    {
                        dishesToRemove.Add(true);
                    }
                    else
                    {
                        dishesToRemove.Add(false);
                    }
                }
                    
                this.orderListView.UnselectAll();

                this.DishRemovedFromOrder(dishesToRemove);
            }
        }

        private void orderListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.orderListView.SelectedItems.Count > 0)
            {
                this.removeDishButton.IsEnabled = true;
            }
            else
            {
                this.removeDishButton.IsEnabled = false;
            }
        }

        private void soupsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.soupsListView.SelectedItem != null)
            {
                this.addDishButton.IsEnabled = true;

                this.selectedDish = DeepClone.CopyObject((Dish)this.soupsListView.SelectedItem);

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }
            }
        }

        private void drinksListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.drinksListView.SelectedItem != null)
            {
                this.addDishButton.IsEnabled = true;

                this.selectedDish = DeepClone.CopyObject((Dish)this.drinksListView.SelectedItem);

                if (this.SelectedDishUpdated != null)
                {
                    this.SelectedDishUpdated(this.selectedDish);
                }
            }
        }

        private void ordersHistoryListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ordersHistoryListView.SelectedItem != null)
            {
                OrderData orderData = (OrderData)(this.ordersHistoryListView.SelectedItem);

                this.historyOrderPriceLabel.Content = "Całkowita cena: " + orderData.TotalPrice + " zł.";

                this.historyOrderCommentsTextBox.Text = orderData.Comments;

                this.orderDataHistoryListView.ItemsSource = orderData.Dishes;
            }
        }
    }
}
