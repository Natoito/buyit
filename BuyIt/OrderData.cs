﻿using BuyIt.Dishes;

using System;
using System.Collections.Generic;

namespace BuyIt
{
    [Serializable]
    public class OrderData
    {
        public List<Dish> Dishes { get; protected set; }
        public float TotalPrice { get; protected set; }
        public string Date { get; protected set; }
        public string Comments { get; set; }

        public OrderData()
        {
            this.Date   = string.Empty;
            this.Dishes = new List<Dish>();

            this.Comments   = string.Empty;
            this.TotalPrice = 0.0f;         
        }

        public OrderData(List<Dish> dishes, float totalPrice, string date, string comments)
        {
            this.Date       = date;
            this.Dishes     = dishes;
            this.Comments   = comments;
            this.TotalPrice = totalPrice;
        }
    }
}
