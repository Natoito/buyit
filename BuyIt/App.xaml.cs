﻿using BuyIt.Dishes;
using BuyIt.Exceptions;

using System;
using System.Windows;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace BuyIt
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		private MainWindow mainWindow;
        private WaitForSendOrderWindow waitForSendOrderWindow;

        private OrderSender orderSender;

        private Menu menu;
        private Order order;
        private OrderHistoryManager orderHistoryManager;

        private Dish actualDish;
        
        private bool orderSending;
        private bool orderSended;

        public App()
        {
            this.order = new Order();
            
            this.orderSended  = false;
            this.orderSending = false;

            this.InitMenu();

            this.InitMainWindow();
            this.InitWaitForSendOrderWindow();

            this.InitOrderSender();

            this.InitOrderHistoryManager();

            this.mainWindow.Show();
        }

        private void InitOrderHistoryManager()
        {
            this.orderHistoryManager = new OrderHistoryManager("orderHistory.xml");
            this.orderHistoryManager.LoadHistory();

            this.mainWindow.FillHistory(this.orderHistoryManager.Orders);
        }

        private void InitMainWindow()
        {
            this.mainWindow = new MainWindow();

            this.mainWindow.FillSoups(this.menu.Soups);
            this.mainWindow.FillDrinks(this.menu.Drinks);
            this.mainWindow.FillPizzas(this.menu.Pizzas);
            this.mainWindow.FillMainDishes(this.menu.MainDishes);
            this.mainWindow.FillPizzasExtras(this.menu.PizzaExtras);
            this.mainWindow.FillMainDishesExtras(this.menu.MainDishesExtras);

            this.mainWindow.DishRemovedFromOrder += RemoveDishesFromOrder; ;
            this.mainWindow.DishUnselected         += CleerActualDish;
            this.mainWindow.DishAddedToOrder       += UpdateOrder;
            this.mainWindow.SelectedDishUpdated    += UpdateActualDish;
            this.mainWindow.SendOrderButtonClicked += SendOrder;

            this.mainWindow.Closed  += MainWindow_Closed;
            this.mainWindow.Closing += MainWindow_Closing;
        }

        private void InitWaitForSendOrderWindow()
        {
            this.waitForSendOrderWindow = new WaitForSendOrderWindow();
        }
        private void InitOrderSender()
        {
            try
            {
                this.orderSender = new OrderSender();
                this.orderSender.Init();

                this.orderSender.OnSendOrderCompleted += OrderSender_OnSendOrderCompleted;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);

                MessageBoxResult result = MessageBox.Show("Nie udało się załadować plików konfiguracyjnych.\nJeżeli błąd będzie się powtarzał, zainstaluj aplikację na nowo.", "Błąd ładowania konfiguracji", MessageBoxButton.OK, MessageBoxImage.Error);

                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }           
        }
        private void InitMenu()
        {
            try
            {
                this.menu = new Menu("menu.xml");
                this.menu.LoadMenu();
            }
            catch (MenuException e)
            {
                Debug.WriteLine(e);

                MessageBoxResult result = MessageBox.Show("Nie udało się załadować menu.\nJeżeli błąd będzie się powtarzał, zainstaluj aplikację na nowo.", "Błąd ładowania menu", MessageBoxButton.OK, MessageBoxImage.Error);

                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void UpdateActualDish(Dish selectedDish)
        {
            Debug.Assert(selectedDish != null, "Selected dish is null");

            this.actualDish = selectedDish;

            this.mainWindow.UpdatePriceToAdd(this.order.TotalPrice, this.order.CalculateDishPrice(this.actualDish));
        }

        private void RemoveDishesFromOrder(List<bool> dishesToRemove)
        {
            Debug.Assert(dishesToRemove.Count == this.order.Dishes.Count, "Amount of dishes not equal");

            for (int i = 0; i < dishesToRemove.Count; i++)
            {
                if(dishesToRemove[i])
                {
                    this.order.RemoveDishByIndex(i);

                    dishesToRemove.RemoveAt(i);
                    i--;
                }
            }

            this.mainWindow.UpdateOrderList(this.order);
            this.mainWindow.UpdatePrice(this.order.TotalPrice);

        }

        private void CleerActualDish()
        {
            this.actualDish = null;
        }

        private void UpdateOrder(Dish selectedDish)
        {
            this.actualDish = null;

            this.order.AddDish(DeepClone.CopyObject(selectedDish));

            this.mainWindow.UpdatePrice(this.order.TotalPrice);
            this.mainWindow.UpdateOrderList(this.order);
        }

        private async void SendOrder(string commentsToOrder)
        {
            this.order.Comments = commentsToOrder;
            this.order.UpdateDate();

            this.actualDish = null;

            this.waitForSendOrderWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.waitForSendOrderWindow.Show();

            this.orderSended  = false;
            this.orderSending = true;

            try
            {
                await this.orderSender.SendOrder(this.order);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);

                MessageBoxResult result = MessageBox.Show("Nie udało się wysłać zamówienia.\nSprawdź swoje połączenie z internetem i/lub spróbuj poźniej.", "Błąd wysyłania zamówienia", MessageBoxButton.OK, MessageBoxImage.Error);

                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void OrderSender_OnSendOrderCompleted(Exception error)
        {
            this.orderSended  = true;
            this.orderSending = false;

            this.waitForSendOrderWindow.Hide();

            if (error == null)
            {
                this.orderHistoryManager.UpdateHistroy(this.order);
                
                this.order.CleanOrder();
                this.mainWindow.PrepareToNextOrder();

                this.mainWindow.FillHistory(this.orderHistoryManager.Orders);
            }
            else
            {
                Debug.WriteLine(error);

                MessageBoxResult result = MessageBox.Show("Nie udało się wysłać zamówienia.\nSprawdź swoje połączenie z internetem i/lub spróbuj poźniej.", "Błąd wysyłania zamówienia", MessageBoxButton.OK, MessageBoxImage.Error);

                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if ((this.orderSending == true) &&
                (this.orderSended == false))
            {
                e.Cancel = true;
            }
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

    }

}
