﻿using System;
using System.Collections.Generic;

namespace BuyIt.Dishes
{
    [Serializable]
    public class ExtendedDish : Dish
    {
        public List<Dish> Extras { get; set; }

        public ExtendedDish(int id, string name, float price)
            : base(id, name, price)
        {
            this.Extras = new List<Dish>();
        }

        public ExtendedDish(string name, float price)
            : base(name, price)
        {
            this.Extras = new List<Dish>();
        }
        
        public override string ToString()
        {
            string ret = this.Id + ". " + this.Name + ", cena(PLN): " + this.Price;

            if (this.Extras.Count > 0)
            {
                ret += "\nDodatki: \n";

                foreach (Dish extra in this.Extras)
                {
                    ret += "- " + extra.Name + ", cena(PLN): " + extra.Price + "\n";
                }
            }

            ret += "\nCałkowita cena(PLN): " + this.TotalPrice;

            return ret;
        }
    }
}
