﻿using System;

namespace BuyIt.Dishes
{
    [Serializable]
    public class Dish
    {
        public int Id { get; }
        public string Name { get; }
        public float Price { get; }
        public float TotalPrice { get; set; }

        private static int LAST_ID = 0;

        public Dish(int id, string name, float price)
        {
            this.Id         = id;
            this.Name       = name;
            this.Price      = price;
            this.TotalPrice = price;
        }

        public Dish(string name, float price)
        {
            this.Name       = name;
            this.Price      = price;
            this.TotalPrice = price;

            this.Id = ++LAST_ID;
        }

        public override string ToString()
        {
            return this.Id + ". " + this.Name + ", cena(PLN): " + this.TotalPrice;
        }
    }
}
