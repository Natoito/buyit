﻿using BuyIt.Exceptions;

using System;
using System.Xml;
using System.Collections.Generic;

namespace BuyIt.Dishes
{
    public class Menu
    {
        /*----------------- public properties -----------------*/

        public List<ExtendedDish> Pizzas { get; private set; }
        public List<Dish> PizzaExtras { get; private set; }

        public List<ExtendedDish> MainDishes { get; private set; }
        public List<Dish> MainDishesExtras { get; private set; }

        public List<Dish> Soups { get; private set; }
        public List<Dish> Drinks { get; private set; }

        /*----------------- private variables -----------------*/

        private string pathToMenu;

        /*----------------- private methods -----------------*/

        /// <summary>
        /// Constructor with path to menu initalization
        /// </summary>
        /// <param name="path"> Path to menu </param>
        public Menu(string path)
        {
            this.pathToMenu = path;

            this.Soups            = new List<Dish>();
            this.Drinks           = new List<Dish>();
            this.Pizzas           = new List<ExtendedDish>();
            this.MainDishes       = new List<ExtendedDish>();
            this.PizzaExtras      = new List<Dish>();
            this.MainDishesExtras = new List<Dish>();
    }

        /// <summary>
        /// Menu initialization.
        /// <exception cref="MenuNotLoadedException"> Thrown when cannot load menu </exception>
        /// <exception cref="MenuNotParsedException"> Thrown when cannot parse menu </exception>
        /// </summary>
        public void LoadMenu()
        {
            try
            {
                XmlDocument data = null;

                data = XmlLoader.LoadFromFile(this.pathToMenu);

                if (data != null)
                {
                    this.ParseData(data);
                }              
            }
            catch (MenuNotParsedException e)
            {
                throw new MenuNotParsedException("Menu not parsed", e);
            }
            catch (XmlLoaderException e)
            {
                throw new MenuNotLoadedException("Menu not loaded", e);
            }
        }

        /*----------------- private methods -----------------*/

        private void ParseData(XmlDocument data)
        {
            if (data.DocumentElement != null)
            {
                if (data.DocumentElement.Name.Equals("menu"))
                {
                    try
                    {
                        foreach (XmlNode child in data.DocumentElement.ChildNodes)
                        {
                            if (child.Name.Equals("pizzas"))
                            {
                                this.ParsePizzas(child);
                            }

                            if (child.Name.Equals("mainDishes"))
                            {
                                this.ParseMainDishes(child);
                            }

                            if (child.Name.Equals("soups"))
                            {
                                this.ParseSoups(child);
                            }

                            if (child.Name.Equals("drinks"))
                            {
                                this.ParseDrinks(child);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                else
                {
                    throw new MenuNotParsedException("Menu section not found");
                }
            }
            else
            {
                throw new MenuNotParsedException("Data not parsed");
            }
        }

        private void ParsePizzas(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("pizza"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        this.Pizzas.Add(new ExtendedDish(name, price));
                    }
                    if (child.Name.Equals("extras"))
                    {
                        ParsePizzaExtras(child);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        private void ParsePizzaExtras(XmlNode node)
        {
            float extraPrice = 0.0f;

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("Price"))
                    {
                        if (float.TryParse(child.FirstChild.Value, out extraPrice) == false)
                        {
                            throw new MenuNotParsedException("Pizza extras price not parsed");
                        }
                    }

                    if (child.Name.Equals("extra"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        if (Math.Abs(price - 0.0f) < float.Epsilon)
                        {
                            price = extraPrice;
                        }

                        this.PizzaExtras.Add(new Dish(name, price));
                    }                                 
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseMainDishes(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("dish"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        this.MainDishes.Add(new ExtendedDish(name, price));
                    }
                    if (child.Name.Equals("extras"))
                    {
                        ParseMainDishExtras(child);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        private void ParseMainDishExtras(XmlNode node)
        {
            float extraPrice = 0.0f;

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("Price"))
                    {
                        if (float.TryParse(child.FirstChild.Value, out extraPrice) == false)
                        {
                            throw new MenuNotParsedException("Main dish extras price not parsed");
                        }
                    }

                    if (child.Name.Equals("extra"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        if (Math.Abs(price - 0.0f) < float.Epsilon)
                        {
                            price = extraPrice;
                        }

                        this.MainDishesExtras.Add(new Dish(name, price));
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseSoups(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("soup"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        this.Soups.Add(new Dish(name, price));
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseDrinks(XmlNode node)
        {
            float extraPrice = 0.0f;

            foreach (XmlNode child in node.ChildNodes)
            {
                try
                {
                    if (child.Name.Equals("Price"))
                    {
                        if (float.TryParse(child.FirstChild.Value, out extraPrice) == false)
                        {
                            throw new MenuNotParsedException("Drinks price not parsed");
                        }
                    }

                    if (child.Name.Equals("drink"))
                    {
                        string name;
                        float price = 0.0f;

                        ParseDish(child, out name, out price);

                        if (Math.Abs(price - 0.0f) < float.Epsilon)
                        {
                            price = extraPrice;
                        }

                        this.Drinks.Add(new Dish(name, price));
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void ParseDish(XmlNode node, out string name, out float price)
        {
            name  = string.Empty;
            price = 0.0f;

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name.Equals("Name"))
                {
                    name = (string)child.FirstChild.Value.Clone();
                }
                if (child.Name.Equals("Price"))
                {
                    if (float.TryParse(child.FirstChild.Value, out price) == false)
                    {
                        throw new MenuNotParsedException("Dish price not parsed");
                    }
                }
            }

            if (name.Equals(string.Empty))
            {
                throw new MenuNotParsedException("Name parameter not found");
            }
        }
    }
}
